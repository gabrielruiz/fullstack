DROP TABLE IF EXISTS `school_class_students`;
DROP TABLE IF EXISTS `teacher`;
DROP TABLE IF EXISTS `student`;
DROP TABLE IF EXISTS `school_class`;
DROP TABLE IF EXISTS `school_test`;
DROP TABLE IF EXISTS `student_grade`;


CREATE TABLE `teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `username` varchar(150) NOT NULL,
  `name` varchar(30) NOT NULL,
  `lastname_1` varchar(30) NOT NULL,
  `created` datetime(6) DEFAULT NULL,
  `updated` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
);

CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `lastname_1` varchar(100) NOT NULL,
  `lastname_2` varchar(100) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `birthday` date DEFAULT NULL,
  `created` datetime(6) DEFAULT NULL,
  `updated` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `school_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(200) NOT NULL,
  `code` varchar(20) NOT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `year` smallint(5) unsigned NOT NULL,
  `created` datetime(6) DEFAULT NULL,
  `updated` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `school_class_code_unique` (`code`),
  KEY `school_class_fk_teacher_id` (`teacher_id`),
  CONSTRAINT `school_class_fk_teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`)
);

CREATE TABLE `school_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_datetime` datetime(6) NOT NULL,
  `school_class_id` int(11) NOT NULL,
  `created` datetime(6) DEFAULT NULL,
  `updated` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `school_test_fk_school_class_id` (`school_class_id`),
  CONSTRAINT `school_test_fk_school_class_id` FOREIGN KEY (`school_class_id`) REFERENCES `school_class` (`id`)
);

CREATE TABLE `student_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` smallint(5) unsigned DEFAULT NULL,
  `created` datetime(6) DEFAULT NULL,
  `updated` datetime(6) DEFAULT NULL,
  `student_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_grade_fk_student_id` (`student_id`),
  KEY `student_grade_fk_test_id` (`test_id`),
  CONSTRAINT `student_grade_fk_student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`),
  CONSTRAINT `student_grade_fk_test_id` FOREIGN KEY (`test_id`) REFERENCES `school_test` (`id`)
);

CREATE TABLE `school_class_students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_class_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `students_id_school_class_id_uniq` (`school_class_id`,`student_id`),
  KEY `school_class_fk_student_id` (`student_id`),
  CONSTRAINT `school_class_student_fk_school_class_id` FOREIGN KEY (`school_class_id`) REFERENCES `school_class` (`id`),
  CONSTRAINT `school_class_student_fk_student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`)
);
