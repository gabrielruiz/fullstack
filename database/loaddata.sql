INSERT INTO student (name, lastname_1, lastname_2, gender)
VALUES ('Name_0', 'Lastname_0', 'Lastname_0', 'm'),
('Name_1', 'Lastname_1', 'Lastname_1', 'm'),
('Name_2', 'Lastname_2', 'Lastname_2', 'm'),
('Name_3', 'Lastname_3', 'Lastname_3', 'm'),
('Name_4', 'Lastname_4', 'Lastname_4', 'm'),
('Name_5', 'Lastname_5', 'Lastname_5', 'm');


INSERT INTO teacher (username, password, name, lastname_1)
VALUES ('teacher_0', '2312d3123', 'teacher_0', 'lastname_t_0'),
('teacher_1', '2312d3123', 'teacher_1', 'lastname_t_1');


INSERT INTO school_class (teacher_id, subject, code, year)
VALUES (1, 'programación', '1-a', 2017),
(2, 'algebra', '1-b', 2017);


INSERT INTO school_test (school_class_id, test_datetime)
VALUES (1, '2017-08-12 12:00:00'),
(1, '2017-08-13 12:00:00'),
(2, '2017-08-12 12:00:00'),
(2, '2017-08-13 12:00:00');


INSERT INTO student_grade (student_id, test_id, value)
VALUES
(1, 1, 80),
(2, 1, 50),
(3, 1, 40),
(1, 2, 90),
(2, 2, 10),
(3, 2, 30),
(1, 3, 90),
(2, 3, 10),
(3, 3, 30),
(4, 3, 40),
(5, 3, 50);


INSERT INTO school_class_students (school_class_id, student_id)
VALUES (1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5);