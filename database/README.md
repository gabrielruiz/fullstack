# Modelo de datos y SQL

## Estructura de archivos

* El modelo de datos se encuentra en el archivo `models.sql`.
* Existe data inicial para poblar el modelo en el archivo `loaddata.sql`.
* Las querys para solucionar las 5 preguntas de SQL están en los archivos `querys.sql` y `question_5.sql`.

## Soluciones modelos de datos.

Con respecto a las Querys para solucionar las preguntas acerca de SQL, las respuestas de las primeras cuatro preguntas están escritas en el archivo `querys.sql`, mientras que la quinta pregunta se respondió realizando una prueba directamente, la que está escrita en el archivo `question_5.sql`.

Para cada pregunta, entonces:

1. Escriba una Query que entregue la lista de  alumnos para el curso
"programación"

    ```
    SELECT student.id AS ID, student.name AS Nombre,
        student.lastname_1 AS Apellido
        FROM student
        INNER JOIN school_class_students
            ON student.id = school_class_students.student_id
        INNER JOIN school_class
            ON school_class_students.school_class_id = school_class.id
        WHERE school_class.subject LIKE 'programación';
    ```

2. Escriba una Query que calcule el promedio de notas de un alumno en un
curso. **(Se realizó pensando en el curso de programación, para el estudiante con ID 1)**

    ```
    SELECT AVG(student_grade.value) AS Promedio FROM student_grade
        INNER JOIN student
            ON student.id = student_grade.student_id
        INNER JOIN school_test
            ON school_test.id = student_grade.test_id
        INNER JOIN school_class
            ON school_test.school_class_id = school_class.id
        WHERE school_class.subject LIKE 'programación'
            AND student.id = 1;
    ```


3. Escriba una Query que entregue a los alumnos y el promedio que tiene
en cada curso.

    ```
    SELECT t2.id as ID, t2.name as Nombre, t2.lastname_1 as Apellido,
        t4.subject as Curso, AVG(t2.value) as Promedio
        FROM
        (SELECT t1.id, t1.name, t1.lastname_1, t0.value, t0.test_id
            FROM student_grade t0
            INNER JOIN student t1
            ON t0.student_id = t1.id) t2
        INNER JOIN school_test t3
            ON t2.test_id = t3.id
        INNER JOIN school_class t4
            ON t3.school_class_id = t4.id
        GROUP BY t2.id, t4.subject;
    ```

4. Escriba una Query que lista a todos los alumnos con más de un curso con
promedio rojo. **(Se considera que los promedios van de 0 a 100, y que 50 es el mínimo de aprobación)**.

    ```
    SELECT t6.id as ID, t6.name as Nombre, t6.lastname_1 as Apellido,
        COUNT(t6.average) AS Reprobadas
    FROM
    (SELECT t2.id, t2.name, t2.lastname_1,
        t4.subject, AVG(t2.value) as average
        FROM
        (SELECT t1.id, t1.name, t1.lastname_1, t0.value, t0.test_id
            FROM student_grade t0
            INNER JOIN student t1
            ON t0.student_id = t1.id) t2
        INNER JOIN school_test t3
            ON t2.test_id = t3.id
        INNER JOIN school_class t4
            ON t3.school_class_id = t4.id
        GROUP BY t2.id, t4.subject) t6
    WHERE t6.average < 50
    GROUP BY t6.id
    HAVING COUNT(t6.average) > 1;
    ```


5. Dejando de lado el problema del cólegio se tiene una tabla con información de jugadores de tenis:
`PLAYERS(Nombre, Pais, Ranking)`. Suponga que Ranking es un número de 1 a 100 que es distinto para cada jugador. Si la tabla en un momento dado tiene solo 20 registros, indique cuantos registros tiene la tabla que resulta de la siguiente consulta:

    ```
    La respuesta es b) 190
    ```