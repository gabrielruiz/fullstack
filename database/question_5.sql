/*
 * Pregunta 5:
 * Se crea la tabla para hacer la prueba, se rellena de datos
 * dummy y se ejecuta la consulta.
 * La respuesta es b) 190.
 */

DROP TABLE IF EXISTS `players`;

CREATE TABLE `players` (
  `Nombre` varchar(128) NOT NULL,
  `Pais` varchar(128) DEFAULT NULL,
  `Ranking` int(11) NOT NULL
);


INSERT INTO players (Nombre, Ranking)
VALUES
('Name_0', 0),
('Name_1', 1),
('Name_2', 2),
('Name_3', 3),
('Name_4', 4),
('Name_5', 5),
('Name_6', 6),
('Name_7', 7),
('Name_8', 8),
('Name_9', 9),
('Name_10', 10),
('Name_11', 11),
('Name_12', 12),
('Name_13', 13),
('Name_14', 14),
('Name_15', 15),
('Name_16', 16),
('Name_17', 17),
('Name_18', 18),
('Name_19', 19);


SELECT c1.Nombre, c2.Nombre
FROM players c1, players c2
WHERE c1.Ranking > c2.Ranking;


