/*
 * Pregunta 1: Escriba una Query que entregue la lista de alumnos
 * para el curso "programación"
 */
SELECT student.id AS ID, student.name AS Nombre, student.lastname_1 AS Apellido FROM student
    INNER JOIN school_class_students
    ON student.id = school_class_students.student_id
    INNER JOIN school_class
    ON school_class_students.school_class_id = school_class.id
    WHERE school_class.subject LIKE 'programación';


/*
 * Pregunta 2: Escriba una Query que calcule el promedio de notas de
 * un alumno en un curso.
 * Se asume que se pasa la ID del alumno y del curso.
 */
SELECT AVG(student_grade.value) AS Promedio FROM student_grade
    INNER JOIN student
    ON student.id = student_grade.student_id
    INNER JOIN school_test
    ON school_test.id = student_grade.test_id
    INNER JOIN school_class
    ON school_test.school_class_id = school_class.id
    WHERE school_class.subject LIKE 'programación'
        AND student.id = 1;


/*
 * Pregunta 3: Escriba una Query que entregue a los alumnos y el promedio
 * que tiene en cada curso
 */
SELECT t2.id as ID, t2.name as Nombre, t2.lastname_1 as Apellido,
    t4.subject as Curso, AVG(t2.value) as Promedio
    FROM
    (SELECT t1.id, t1.name, t1.lastname_1, t0.value, t0.test_id
        FROM student_grade t0
        INNER JOIN student t1
        ON t0.student_id = t1.id) t2
    INNER JOIN school_test t3
        ON t2.test_id = t3.id
    INNER JOIN school_class t4
        ON t3.school_class_id = t4.id
    GROUP BY t2.id, t4.subject;


/*
 * Pregunta 4: Escriba una Query que lista a todos los alumnos con más de
 * un curso con promedio rojo.
 */
SELECT t6.id as ID, t6.name as Nombre, t6.lastname_1 as Apellido, COUNT(t6.average) AS Reprobadas
FROM
    (SELECT t2.id, t2.name, t2.lastname_1,
        t4.subject, AVG(t2.value) as average
        FROM
        (SELECT t1.id, t1.name, t1.lastname_1, t0.value, t0.test_id
            FROM student_grade t0
            INNER JOIN student t1
            ON t0.student_id = t1.id) t2
        INNER JOIN school_test t3
            ON t2.test_id = t3.id
        INNER JOIN school_class t4
            ON t3.school_class_id = t4.id
        GROUP BY t2.id, t4.subject) t6
    WHERE t6.average < 50
    GROUP BY t6.id
    HAVING COUNT(t6.average) > 1;

