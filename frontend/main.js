/*jslint browser: true*/ /*global  $*/

var schedulingData = {
    "monday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "09:30", "end_time": "11:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "tuesday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "11:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "wednesday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "10:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "thursday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "09:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "friday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "09:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ]
}

var localizedDays = [
    "Domingo",
    "Lunes",
    "Martes",
    "Miercoles",
    "Jueves",
    "Viernes",
    "Sabado"
];

var unlocalizedDays = [
    "sunday",
    "monday",
    "tuesday",
    "wednesday",
    "thursday",
    "friday",
    "saturday"
];


function isNumber(value) {
    return !isNaN(parseFloat(value))
}

/*
 * Agrega un cero al inicio si el valor entregado representa a un número entre 0 y 9.
 */
function pad(value) {
    return isNumber(value) ? (10 > parseInt(value) && parseInt(value) >= 0 ? "0" + value : value) : value;
}


/*
 * Créditos a StackOverflow
 * Formatea un string en base a argumentos tipo '{0}', muy parecido al
 * uso de '.format()' en Python.
 */
if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) {
      return typeof args[number] != 'undefined'
        ? pad(args[number])
        : match
      ;
    });
  };
}


/*
 * Dibuja las celdas con las horas que aparecen a los costados del
 * calendario. Retorna el objeto que representa a la celda.
 */
function drawTimeCell(gridTime) {
    var cell = $("<div>", {
        class: "col col-time",
    }).html("{0}:{1}".format(gridTime.getHours(), gridTime.getMinutes()));
    return cell
}


/*
 * Dibuja una fila, comenzando por el bloque horario, seguido por
 * siete bloques en blanco y terminando con un nuevo bloque horario.
 * Retorna un objeto que representa a la fila, con todos sus bloques
 * anidados.
 */
function drawRow(gridTime) {
    var startDate = new Date(gridTime.getTime());
    var startCell = drawTimeCell(startDate);
    var endCell = drawTimeCell(startDate);
    var row = $("<div>", {
        class: "row row-blocks",
        id: "row-{0}-{1}".format(startDate.getHours(), startDate.getMinutes())
    });
    row.append(startCell);

    var endDate = new Date(startDate.getTime());
    endDate.setDate(endDate.getDate() + 6);
    while(startDate <= endDate) {
        cell = $("<div>", {
            class: "col col-block col-{0}".format(unlocalizedDays[startDate.getDay()])
        }).html("&nbsp;");
        row.append(cell);
        startDate.setDate(startDate.getDate() + 1);
    }
    row.append(endCell);
    return row;
}


/*
 * Dibuja una fila para la cabecera del calendario. Comienza por las
 * flechasde desplazamiento, seguido por siete bloques con el día de
 * la semana, y terminando con un nuevo bloque con fecha de
 * desplazamiento.
 * Retorna un objeto que representa a la fila, con todos sus bloques
 * anidados.
 */
function drawHeader(startDate) {
    // Hace copias de la fecha para no alterar la fecha original.
    startDate = new Date(startDate.getTime());
    var startCell = $("<div>", {
        class: "col col-time"
    }).html($("<i>", {
        class: "fa fa-angle-double-left"
    }));
    var endCell = $("<div>", {
        class: "col col-time"
    }).html($("<i>", {
        class: "fa fa-angle-double-right"
    }));

    var header = $("<div>", {
        class: "row row-days"
    }).html(startCell);

    var endDate = new Date(startDate.getTime());
    endDate.setDate(endDate.getDate() + 6);
    while(startDate <= endDate) {
        let cell = $("<div>", {
            class: "col col-block"
        }).html("{0} {1}".format(
            localizedDays[startDate.getDay()],
            startDate.getDate())
        );
        header.append(cell);
        startDate.setDate(startDate.getDate() + 1);
    }
    header.append(endCell);
    return header;
}


/*
 * Dibuja la grilla que representa al calendario, dentro de un
 * div. Actualmente dibuka la grilla desde 8 am hasta 20 hrs
 * (último bloque comienza a las 19.30).
 * Retorna un objeto que representa a ese div, de forma que pueda
 * ser insertado en cualquier otro espacio.
 */
function createGrid() {
    var scheduleContainer = $("<div>", {
    });
    var gridTime = new Date();
    gridTime.setHours(8);
    gridTime.setMinutes(0);
    gridTime.setSeconds(0);
    var gridEndTime = new Date(gridTime.getTime());
    gridEndTime.setHours(20);
    var header = drawHeader(gridTime);
    scheduleContainer.append(header);

    while (gridTime < gridEndTime) {
        let row = drawRow(gridTime);
        scheduleContainer.append(row);
        gridTime.setMinutes(gridTime.getMinutes() + 30);
    }
    return scheduleContainer;
}


/*
 * Rellena una celda en particular con los datos de un item.
 * El item debe detallar la hora de inicio, de término y el
 * nombre del usuario.
 * Recibe por parámetro el item y la clase de la columna
 * donde debe ubicarse.
 */
function fillCell(item, col) {
    var itemTime = item.start_time.split(":");
    var startTime = new Date();
    startTime.setHours(itemTime[0]);
    startTime.setMinutes(itemTime[1]);

    itemTime = item.end_time.split(":");
    var endTime = new Date();
    endTime.setHours(itemTime[0]);
    endTime.setMinutes(itemTime[1]);
    while(startTime < endTime) {
        let row = $("#row-{0}-{1}".format(startTime.getHours(), startTime.getMinutes()));
        row.children(col).html(item.name).addClass("col-used");
        startTime.setMinutes(startTime.getMinutes() + 30);
    }
}


/*
 * Carga los datos desde un objeto JSON para renderizarlos
 * en grilla.
 * Recibe como parámetro la data de las horas ocupadas.
 */
function loadData(data) {
    $.each(data, function (key, items) {
        let dayCol = ".col-{0}".format(key);
        for (let item of items) {
            fillCell(item, dayCol);
        }
    });
}


/*
 * Función principal del sistema. Es la encargada de renderizar el
 * calendario y mostrar los datos.
 *
 * Flujo:
 * - Dibujar grilla
 *   - Dibuja cabecera
 *   - Crea cada bloque de una fila.
 *   - Agrega los bloques a una fila.
 *   - Agrega las filas consecutivamente al final del calendario.
 * - Leer datos desde el objeto JSON.
 * - Altera los bloques definidos como ocupados, agregando una clase
 *   y mostrando el nombre de quien ocupa el bloque.
 */
function drawCalendar(data) {
    var container = $("#schedule");
    var grid = createGrid();
    container.html(grid);
    loadData(data);
}


$(document).ready(function(){
    drawCalendar(schedulingData);
});
