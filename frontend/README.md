## Supuestos

* Dado que la fecha es irrelevante en la data recibida, se hizo que el calendario comenzara en el día actual, por lo que la primera columna no es necesariamente el día Lunes.
* Las horas disponibles van desde las 8 hrs. hasta las 20 hrs., siendo el último bloque disponible el que comienza a las 19.30 hrs.
* En la data, cuando se habla de `end_time`, se refiere a la hora de término de los bloques ocupados, por lo que tener un hora de término a las 15 hrs, implica que se ocupan los bloques *hasta* las 14.30, inclusive, pero no el de las 15 hrs.


## Dependencias

Los paquetes de los que depende el proyecto están declarados en el `head` de `home.html` mediante un enlace al respectivo CDN, por lo que requiere de internet para funcionar. Los paquetes son:

* Bootstrap 3.3.7
* Font-awesome 4.7.0
* jQuery 3.2.1

## Ejecución

* Abrir archivo `home.html`

## Función principal

En el archivo `main.js`, la función principal que se invoca es `drawCalendar`, la que recibe como parámetro la data de las horas ocupadas.