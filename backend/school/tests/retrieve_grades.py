# -*- coding: utf-8 -*-
from django.db.models import Avg
from django.test import TestCase
from school.models import Student, StudentClassGrade, SchoolClass
from django.contrib.auth.models import User
from django.utils import timezone


class RetrieveGradesTestCase(TestCase):
    """
    Tests para verificar que se crean los promedios finales de
    los alumnos y que pueden ser buscados desde la base de datos.

    :Authors:
        - Gabriel Ruiz

    :Last Modification:
        - 11.08.2017
    """

    def setUp(self):
        """
        Debe agregar una cantidad X de usuarios con promedio de notas,
        de los cuales, Y tienen promedio rojos.

        :Last Modification:
            - 11.08.2017
        """
        teacher = User.objects.create(first_name='Juan',
                                      last_name='Perez',
                                      username='profesor_a')
        school_class = SchoolClass.objects.create(subject='programación',
                                                  code='0012-B',
                                                  teacher=teacher,
                                                  year=timezone.now().year)
        for i in range(1, 11):
            student = Student.objects.create(
                name='Student_{}'.format(i),
                lastname_1='Lastname_{}'.format(i),
                gender='m')
            StudentClassGrade.objects.create(student=student,
                                             school_class=school_class,
                                             value=i*10)


    def test_can_retrieve_all_grades(self):
        """
        Debe poder obtener la cantidad X de usuarios con promedio
        que fueron agregados en la etapa de setUp.
        Sabemos que hay 10 usuarios creados y a los que se le
        agregaron promedios de curso, así que debe haber 10 usuarios
        con promedios finales.

        :Last Modification:
            - 11.08.2017
        """
        students = Student.objects.all().annotate(
            final_average=Avg('class_grades__value'))
        self.assertEqual(students.count(), 10)

        # Verificamos que sean los mismos alumnos creados al principio.
        for student in students:
            self.assertNotEqual(student.final_average, 0)

    def test_can_retrieve_red_grades(self):
        """
        Debe encontrar la misma cantidad de usuarios Y con promedio rojo
        que fueron creados en la etapa de setUp.

        Consideramos que el promedio de aprobación es 50, aunque después se
        debe poder agregar ese valor a los settings del proyecto. Debería
        encontrar 4 alumnos con ese promedio.

        :Last Modification:
            - 11.08.2017
        """
        students = Student.objects.all().annotate(
            final_average=Avg('class_grades__value'))
        self.assertEqual(students.count(), 10)

        # Verificamos que sean los mismos alumnos creados al principio.
        # Deben ser los estudiantes con Student_<ID>, con ID < 5.
        for i in range(1, 11):
            student = students.get(name='Student_{}'.format(i))
            if i < 5:
                self.assertLess(student.final_average, 50)
            else:
                self.assertGreaterEqual(student.final_average, 50)
