# -*- coding: utf-8 -*-
"""
Los tests escritos en este paquete son bastante simples:

* write_grades.py: Comprobar que el promedio de un alumno se actualiza correctamente
al agregar una nota.
* retrieve_grades.py: Comprobar que se puede obtener una lista de usuarios con sus
promedios generales y una lista de usuarios con promedios rojos.
* api_test.py: Comprobar que las API se pueden llamar correctamente y se respeta la
configuración de permisos definida en settings.

IMPORTANTE: Los tests han sido separados en distintos archivos (estilo Java)
para poder realizar una mantención más simple y no navegar por miles de líneas
de código, como ocurriría si fuese un proyecto más grande.

:Authors:
    - Gabriel Ruiz

:Last Modification:
    - 2017.08.15
"""
from .write_grades import WriteGradesTestCase
from .retrieve_grades import RetrieveGradesTestCase
from .api_test import ApiTestCase
