# -*- coding: utf-8 -*-
from django.test import TestCase, Client


class ApiTestCase(TestCase):
    """
    Tests para verificar que, sin estar autenticado, se pueden leer
    los datos del sistema, pero no se puede escribir.

    :Authors:
        - Gabriel Ruiz

    :Last Modification:
        - 14.08.2017
    """
    fixtures = ['users.json']

    def setUp(self):
        self.client = Client()

    def test_can_get_students(self):
        response = self.client.get('/api/students/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 5)

        response = self.client.get('/api/classes/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 2)

        response = self.client.get('/api/tests/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 4)

        response = self.client.get('/api/grades/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 16)

        response = self.client.get('/api/classgrades/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 8)

        response = self.client.get('/api/finalgrades/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 5)

    def test_can_create_students(self):
        data = {
            'name': 'Panchito',
            'lastname_1': 'del rancho',
            'gender': 'm'
        }
        response = self.client.post('/api/students/', data)
        self.assertEqual(response.status_code, 403)

        data = {
            'subject': 'Geometría galáctica',
            'code': '1-b',
            'teacher_id': 1
        }
        response = self.client.post('/api/classes/', data)
        self.assertEqual(response.status_code, 403)

        data = {
            'school_class': 1,
            'datetime': '2017-09-01 10:00:00'
        }
        response = self.client.post('/api/tests/', data)
        self.assertEqual(response.status_code, 403)

        data = {
            'test': 1,
            'student': 1,
            'value': 20
        }
        response = self.client.post('/api/grades/', data)
        self.assertEqual(response.status_code, 403)

        data = {
            'school_class': 1,
            'student': 1,
            'value': 50
        }
        response = self.client.post('/api/classgrades/', data)
        self.assertEqual(response.status_code, 403)

        data = {
            'student': 1,
            'value': 50,
            'year': 2017
        }
        response = self.client.post('/api/classgrades/', data)
        self.assertEqual(response.status_code, 403)



