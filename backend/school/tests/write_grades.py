# -*- coding: utf-8 -*-
from django.test import TestCase
from school.models import Student, StudentClassGrade, StudentGrade,\
    SchoolTest, SchoolClass
from django.contrib.auth.models import User
from django.utils import timezone


class WriteGradesTestCase(TestCase):
    """
    Tests para verificar que se pueden guardar notas para los alumnos
    y se crean los promedios por curso inmediatamente.

    :Authors:
        - Gabriel Ruiz

    :Last Modification:
        - 11.08.2017
    """

    def setUp(self):
        """
        Crea los datos iniciales en la BD de prueba

        :Last Modification:
            - 11.08.2017
        """
        teacher = User.objects.create(first_name='Juan',
                                      last_name='Perez',
                                      username='profesor_a')
        school_class = SchoolClass.objects.create(subject='programación',
                                                  code='0012-B',
                                                  teacher=teacher,
                                                  year=timezone.now().year)
        SchoolTest.objects.create(school_class=school_class,
                                  datetime=timezone.now())
        student = Student.objects.create(name='Gonzalo',
                                         lastname_1='Gonzalez',
                                         gender='m')
        school_class.students.add(student)

    def test_can_update_grades(self):
        """
        Prueba que se pueda escribir una nota de una prueba de una alumno
        y que el promedio del curso y general de un alumno se actualiza.

        Las llamadas a la base de datos se hacen en forma secuencial (no
        optimizada) para poder chequear el éxito o fracaso de cada una.

        :Last Modification:
            - 11.08.2017
        """
        school_class = SchoolClass.objects.get(subject='programación')
        school_test = school_class.tests.first()
        student = school_class.students.first()
        StudentGrade.objects.create(student=student,
                                    test=school_test,
                                    value=80)

        # Chequea que el promedio se haya actualizado a 80
        student_class_grade = StudentClassGrade.objects.get(
            student=student,
            school_class=school_class)
        self.assertEqual(student_class_grade.value, 80)

        StudentGrade.objects.create(student=student,
                                    test=school_test,
                                    value=60)

        # Chequea que el promedio se haya actualizado a 70
        student_class_grade = StudentClassGrade.objects.get(
            student=student,
            school_class=school_class)
        self.assertEqual(student_class_grade.value, 70)
