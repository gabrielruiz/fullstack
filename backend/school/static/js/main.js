function clearGrid(containerId) {
    var rows = $(containerId).find(".row-list");
    rows.remove();
}

function drawFinalGrades(data, containerId) {
    var container = $(containerId);
    for (let item of data) {
        let row = $("<div>", {
            class: "row row-list",
            value: item.value
        });
        let colYear = $("<div>", {
            class: "col col-xs-3 col-year",
        }).html(item.year);

        let colName = $("<div>", {
            class: "col col-xs-3 col-name"
        }).html("{0} {1}".format(item.student.name, item.student.lastname_1));

        let colGrade = $("<div>", {
            class: "col col-xs-3 col-grade"
        }).html(item.value);

        let colApproved = $("<div>", {
            class: "col col-xs-3 col-approved"
        }).html("{0}/{1} cursos".format(item.classes_qty - item.failing_qty, item.classes_qty));

        row.append(colYear).append(colName).append(colGrade).append(colApproved);
        container.append(row);
    }
}


/*
 * Accede a la API para buscar la data de las notas.
 * Si la encuentra, agrega las filas a la lista.
 */
function loadData(containerId) {
    $.ajax({
        method: "GET",
        url: "/api/finalgrades/"
    }).done(function (data) {
        localStorage.setItem('finalGrades', JSON.stringify(data));
        drawFinalGrades(data.results, containerId);
    }).fail(function (response) {
        if (response.status === 400) {
            alert("No se puede acceder al sistema");
        } else {
            alert("Hubo un error al buscar los datos. Intente más tarde.");
        }
    });
}



function drawHeader(containerId) {
    var items = ["Año", "Alumno", "Promedio", "Aprobación"]
    var container = $(containerId);
    var header = $("<div>", {
        class: "row row-header"
    });
    for (let item of items) {
        let cell = $("<div>", {
            class: "col col-xs-3"
        }).html(item);
        header.append(cell);
    }
    container.html(header);
    return header;
}

/*
 * Función principal del sistema. Es la encargada de renderizar el
 * calendario y mostrar los datos.
 *
 * Flujo:
 * - Dibuja cabecera de la grilla (solo usando <div>).
 * - Leer datos desde la API.
 * - Agrega filas columnas consecutivamente con los datos
 *   recibidos.
 */
function drawList(containerId) {
    var header = drawHeader(containerId);
    loadData(containerId);
}


$(document).ready(function(){
    var containerId = "#gradesContainer";
    drawList(containerId);

    $(this).on("click", "a.failing-filter", function (event) {
        event.preventDefault();
        var data = JSON.parse(localStorage.getItem('finalGrades')).results;
        var failingQty = $(this).data('failing');
        var filteredData = $.grep(data, function (obj, index ) {
            if (failingQty == 1) {
                return obj.failing_qty === 1;
            } else if (failingQty == 2) {
                return obj.failing_qty > 1;
            } else {
                return obj;
            }
        });
        clearGrid(containerId);
        drawFinalGrades(filteredData, containerId);
    });
});