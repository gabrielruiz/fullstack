var rowByClass = {
    "students": ["id", "name", "lastname_1"],
    "classes": ["id", "code", "subject", "teacher.display"],
    "tests": ["id", "school_class.display", "datetime"],
    "grades": ["test.display", "student.display", "value"]
}


/*
 * Rellena el formulario en la vista con los datos de
 * un objeto JSON.
 */
function fillForm(data, classname) {
    $.each(data, function(key, value){
        var input = $("#id_{0}".format(key));
        input.val(value);
    });
}


/*
 * Edita una fila con los datos recibidos por
 * parametro.
 */
function editRow(item, classname) {
    var colnames = rowByClass[classname];
    var row = $("#row-{0}".format(item.id));
    row.html("");
    for (let colname of colnames){
        colstring = colname.split(".");
        let value = item;
        for (let part of colstring) {
            value = value[part];
        }
        let col = $("<td>").html(value);
        row.append(col);
    }

    // Boton de eliminacion
    let col = $("<td>", {
        class: "btn btn-link btn-delete",
        "data-object": item.id,
        "data-classname": classname
    }).html($("<i>", {
        class: "fa fa-trash"
    }));
    col.data("object", item.id);
    col.data("classname", classname);
    row.append(col);

    // Boton de edicion
    let col2 = $("<td>", {
        class: "btn btn-link btn-edit",
        "data-object": item.id,
        "data-classname": classname
    }).html($("<i>", {
        class: "fa fa-edit"
    }));
    col2.data("object", item.id);
    col2.data("classname", classname);
    row.append(col2);
}

/*
 * Rellena la tabla con la lista de alumnos.
 * Agrega un botón de elimina y uno de editar
 * en las dos últimas columnas.
 */
function insertRow(data, classname) {
    var colnames = rowByClass[classname];
    container = $("#{0}Container".format(classname));
    for(let item of data) {
        let row = $("<tr>", {
            id: "row-{0}".format(item.id)
        });
        for (let colname of colnames){
            colstring = colname.split(".");
            let value = item;
            for (let part of colstring) {
                value = value[part];
            }
            let col = $("<td>").html(value);
            row.append(col);
        }

        // Boton de eliminacion
        let col = $("<td>", {
            class: "btn btn-link btn-delete",
            "data-object": item.id,
            "data-classname": classname
        }).html($("<i>", {
            class: "fa fa-trash"
        }));
        col.data("object", item.id);
        col.data("classname", classname);
        row.append(col);

        // Boton de edicion
        let col2 = $("<td>", {
            class: "btn btn-link btn-edit",
            "data-object": item.id,
            "data-classname": classname
        }).html($("<i>", {
            class: "fa fa-edit"
        }));
        col2.data("object", item.id);
        col2.data("classname", classname);
        row.append(col2);

        container.prepend(row);
    }
}


/*
 * Rellena las opciones un 'select' dentro de un
 * formulario con los datos recibidos en el objeto
 * JSON 'data'.
 */
function insertOptions(data, object) {
    for (let item of data) {
        let option = $("<option>", {
            value: item.id
        }).html(item.display);
        object.append(option);
    }
}


/*
 * Carga los datos disponibles desde la API,
 * de un objeto definido en data.type, por ejemplo
 * "classes" (Cursos), "users" (profesores), etc.
 */
function loadSelectValues(object) {
    var url = "/api/{0}/".format(object.data("type"));
    $.ajax({
        method: "GET",
        url: url

    }).done(function (resp) {
        insertOptions(resp.results, object)
    }).fail(function (resp) {
         if (response.status === 400) {
            alert("No se puede acceder al sistema");
        } else {
            alert("Hubo un error al buscar los datos. Intente más tarde.");
        }
    });
}


/*
 * Obtiene la lista de objetos de una clase desde
 * la API respectiva.
 */
function loadList(classname) {
    var url = "/api/{0}/".format(classname);
    $.ajax({
        method: "GET",
        url: url
    }).done(function (response) {
        insertRow(response.results, classname)
    }).fail(function (response) {
         if (response.status === 400) {
            alert("No se puede acceder al sistema");
        } else {
            alert("Hubo un error al buscar los datos. Intente más tarde.");
        }
    });
}


$(document).ready(function () {
    /*
     * Envía formulario para crear un nuevo alumno,
     * clase, etc.
     */
    "use strict";
    $(".addition-form").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        $(".error_li").text("");
        var formURL = $(this).attr("action");
        var postData = $(this).serializeArray();
        var isEdition = false;
        var method = "POST";
        for (let obj of postData) {
            if (obj.name == 'id' && obj.value !== "") {
                isEdition = true;
                method = "PUT";
                formURL = "{0}{1}/".format(formURL, obj.value);
            }
        }

        $.ajax({
            method: method,
            url: formURL,
            data: postData
        }).done(function (response) {
            if (isEdition) {
                editRow(response, form.data("classname"));
            } else {
                insertRow([response], form.data("classname"));
            }
        }).fail(function (response) {
            console.log(response.responseJSON);
            $.each(response.responseJSON, function (key, value) {
                form.find("#" + key + "_error").text("value");
            });
        });
    });

    $("select.autoload").each(function () {
        loadSelectValues($(this));
    });

    loadList($("#tableContainer").data("classname"));

    /*
     * Eliminación de un objeto. No solicita confirmación por ahora.
     */
    $(this).on("click", "td.btn-delete", function(event) {
        event.preventDefault();
        var data = $(this).data();
        var url = "/api/{0}/{1}/".format(data.classname, data.object);
        $.ajax({
            method: "DELETE",
            url: url
        }).done(function (response) {
            let row = $("#row-{0}".format(data.object));
            row.remove();
        }).fail(function (response) {
            if (response.status === 400) {
                alert("No se puede acceder al sistema");
            } else {
                alert("Hubo un error al buscar los datos. Intente más tarde.");
            }
        });
    })

    /*
     * Edición de un objeto. Busca los datos del objeto (en base a la
     * clase e ID) y rellena el formulario que está en la página.
     */
    $(this).on("click", "td.btn-edit", function(event) {
        event.preventDefault();
        var data = $(this).data();
        var url = "/api/{0}/{1}/".format(data.classname, data.object);
        $.ajax({
            method: "GET",
            url: url
        }).done(function (response) {
            fillForm(response, data.classname);
        }).fail(function (response) {
            if (response.status === 400) {
                alert("No se puede acceder al sistema");
            } else {
                alert("Hubo un error al buscar los datos. Intente más tarde.");
            }
        });
    })
});



