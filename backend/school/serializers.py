# -*- coding: utf-8 -*-
"""
Serializadores para las clases que conforman el sistema.
En caso de tener una FK a otra tabla, se agrega campos para
lectura y escritura por separado.

El campo de lectura es <field_name> y el de escritura es
<field_name_id>, de tal forma de DRF lo reconozca como campo
del modelo.

:Authors:
    - Gabriel Ruiz

:Last Modification:
    - 15.08.2017
"""

from django.contrib.auth.models import User
from school.models import Student, SchoolClass, SchoolTest, \
    StudentGrade, StudentClassGrade, FinalGrade
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    """
    Clase que crea el string en formato JSON con los datos de un alumno,
    pero incluyendo las notas finales por ramo.
    """
    display = serializers.SerializerMethodField()

    class Meta:
        model = User
        exclude = ('password', 'is_superuser', 'is_staff')

    def get_display(self, obj):
        return "{} {}".format(obj.first_name, obj.last_name)


class StudentSerializer(serializers.ModelSerializer):
    """
    Clase que crea el string en formato JSON con los datos de un alumno.
    Cuenta con la función de crear o restablecer un objeto en base a un
    diccionario de JSON.
    """
    display = serializers.SerializerMethodField()

    class Meta:
        model = Student
        fields = '__all__'

    def get_display(self, obj):
        return str(obj)

class SchoolClassSerializer(serializers.ModelSerializer):
    display = serializers.SerializerMethodField()
    teacher = UserSerializer(read_only=True)
    teacher_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = SchoolClass
        fields = '__all__'

    def get_display(self, obj):
        return str(obj)


class SchoolTestSerializer(serializers.ModelSerializer):
    display = serializers.SerializerMethodField()
    school_class = SchoolClassSerializer(read_only=True)
    school_class_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = SchoolTest
        fields = '__all__'

    def get_display(self, obj):
        return str(obj)


class StudentGradeSerializer(serializers.ModelSerializer):
    display = serializers.SerializerMethodField()
    test = SchoolTestSerializer(read_only=True)
    test_id = serializers.IntegerField(write_only=True)
    student = StudentSerializer(read_only=True)
    student_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = StudentGrade
        fields = '__all__'

    def get_display(self, obj):
        return str(obj)


class StudentClassGradeSerializer(serializers.ModelSerializer):
    student = StudentSerializer()
    school_class = SchoolClassSerializer()
    display = serializers.SerializerMethodField()

    class Meta:
        model = StudentClassGrade
        fields = '__all__'

    def get_display(self, obj):
        return str(obj)


class FinalGradeSerializer(serializers.ModelSerializer):
    """
    Clase que crea el string en formato JSON con los datos de un alumno,
    pero incluyendo las notas finales por ramo.
    """
    student = StudentSerializer()
    display = serializers.SerializerMethodField()

    class Meta:
        model = FinalGrade
        fields = '__all__'

    def get_display(self, obj):
        return str(obj)
