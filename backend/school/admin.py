# -*- coding: utf-8 -*-
from django.contrib import admin
from django.db.models import Avg
from django.utils.translation import ugettext_lazy as _
from school.models import \
    Student, \
    SchoolClass, \
    SchoolTest, \
    StudentGrade, \
    StudentClassGrade


class FailingGradeFilter(admin.SimpleListFilter):
    title = _('FailingGrades')
    parameter_name = 'failing_grade'

    def lookups(self, request, model_admin):
        return ((1, 'Si'), (0, 'No'))

    def queryset(self, request, queryset):
        if self.value() == '1':
            return queryset.filter(final_average__lt=50)
        elif self.value() == '0':
            return queryset.filter(final_average__gte=50)
        return queryset


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ('name', 'lastname_1', 'lastname_2', 'average')
    search_fields = ('name', 'lastname_1', 'lastname_2')
    list_filter = (FailingGradeFilter, )
    save_on_top = True

    def get_queryset(self, request):
        queryset = super(StudentAdmin, self).get_queryset(request)
        return queryset.annotate(final_average=Avg('class_grades__value'))

    def average(self, obj):
        return obj.final_average


@admin.register(SchoolClass)
class SchoolClassAdmin(admin.ModelAdmin):
    list_display = ('subject', 'teacher', 'code')
    list_select_related = ('teacher', )
    save_on_top = True


@admin.register(SchoolTest)
class SchoolTestAdmin(admin.ModelAdmin):
    list_display = ('school_class', 'datetime')
    list_select_related = ('school_class', )
    save_on_top = True


@admin.register(StudentGrade)
class StudentGradeAdmin(admin.ModelAdmin):
    list_display = ('test', 'student', 'value')
    list_select_related = ('test', 'student')
    save_on_top = True


@admin.register(StudentClassGrade)
class StudentClassGradeAdmin(admin.ModelAdmin):
    list_display = ('student', 'school_class', 'value')
    list_select_related = ('teacher', )
    save_on_top = True
