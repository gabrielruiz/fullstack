# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from school import api
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'students', api.StudentViewSet, base_name="students")
router.register(r'classes', api.SchoolClassViewSet, base_name="classes")
router.register(r'tests', api.SchoolTestViewSet, base_name="tests")
router.register(r'grades', api.StudentGradeViewSet, base_name="grades")
router.register(r'classgrades', api.StudentClassGradeViewSet,
                base_name="class_grades")
router.register(r'finalgrades', api.FinalGradeViewSet,
                base_name="final_grades")
router.register(r'users', api.UserViewSet,
                base_name="users")


urlpatterns = [
    url(r'^', include(router.urls)),
]
