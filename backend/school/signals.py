# -*- coding: utf-8 -*-
from django import dispatch
from django.db.models import Avg, Count
from django.db.models.signals import post_save, post_delete
from school.models import StudentGrade, StudentClassGrade, FinalGrade


@dispatch.receiver(post_delete, sender=StudentGrade)
@dispatch.receiver(post_save, sender=StudentGrade)
def grade_post_change_handler(sender, **kwargs):
    """
    Recibe una señal de creación, actualización o eliminación de una
    instancia de StudentGrade.
    Busca o crea el registro de promedio de notas de un alumno en el curso
    en particular y actualiza el valor del promedio.

    :Authors:
        - Gabriel Ruiz

    :Last Modification:
        - 11.08.2017
    """
    student_grade = kwargs['instance']
    school_class = student_grade.test.school_class
    mean_grade = StudentGrade.objects.filter(test__school_class=school_class)\
        .aggregate(Avg('value'))
    StudentClassGrade.objects.update_or_create(
        student=student_grade.student,
        school_class=school_class,
        defaults={'value': mean_grade.get('value__avg')})


@dispatch.receiver(post_save, sender=StudentClassGrade)
def class_grade_post_change_handler(sender, **kwargs):
    """
    Recibe una señal de creación o actualización de una instancia de
    StudentClassGrade.
    Busca o crea el registro de promedio de notas de un alumno en el curso
    en particular y actualiza el valor del promedio.

    :Authors:
        - Gabriel Ruiz

    :Last Modification:
        - 15.08.2017
    """
    class_grade = kwargs['instance']
    year = class_grade.school_class.year
    student = class_grade.student
    mean_grade = student.class_grades.filter(school_class__year=year)\
        .aggregate(Avg('value'), Count('id'))
    failed = student.class_grades.filter(school_class__year=year,
                                         value__lt=50).count()
    FinalGrade.objects.update_or_create(
        student=student,
        year=year,
        defaults={
            'value': mean_grade.get('value__avg'),
            'classes_qty': mean_grade.get('id__count'),
            'failing_qty': failed
        })
