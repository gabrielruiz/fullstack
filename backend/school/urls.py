# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.views.generic.base import TemplateView

app_name = "school"

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name="school/list.html"),
        name="averages_view"),
    url(r'^students$', TemplateView.as_view(template_name="school/students.html"),
        name="students_view"),
    url(r'^classes$', TemplateView.as_view(template_name="school/classes.html"),
        name="classes_view"),
    url(r'^tests$', TemplateView.as_view(template_name="school/tests.html"),
        name="tests_view"),
    url(r'^grades$', TemplateView.as_view(template_name="school/grades.html"),
        name="grades_view")
]



