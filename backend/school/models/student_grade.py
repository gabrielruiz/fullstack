# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from .school_test import SchoolTest
from .student import Student


class StudentGrade(models.Model):
    """
    Modelo de datos para guardar las notas de las pruebas tomadas a
    los alummnos.

    :Authors:
        - Gabriel Ruiz

    :Last Modification:
        - 2017.08.10
    """
    student = models.ForeignKey(Student, related_name='grades')
    test = models.ForeignKey(SchoolTest, related_name='student_grades')
    value = models.PositiveSmallIntegerField(_('Value'), null=True, blank=True)
    created = models.DateTimeField(_('Created'), auto_now_add=True,
                                   editable=False)
    updated = models.DateTimeField(_('Updated'), auto_now=True)

    class Meta:
        app_label = 'school'
        verbose_name = _('StudentGrade')
        verbose_name_plural = _('StudentGrades')
        ordering = ['id']

    def __str__(self):
        return "{} - {}".format(str(self.student),
                                str(self.test))
