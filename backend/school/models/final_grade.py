# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from .student import Student


class FinalGrade(models.Model):
    """
    Modelo de datos para guardar los promedios de notas finales
    para cada alumno. Se crea para no tener que recalcular el
    promedio cada vez que se revisan las notas, sino que solo
    se recalcule cuando haya un cambio en el promedio de un curso.

    Este modelo se crea considerando que, en el caso de uso dado,
    existen muchas más lecturas que escrituras (cada nota debería
    escribirse una vez solamente, no existen varias pruebas del mismo
    curso en el mismo día, etc.) debido a que los alumnos si pueden
    revisar las notas varias veces.

    Se asume que las notas siempre se aproxima, funcionando en
    un rango de 0 a 100, y que los promedios finales son anuales.

    :Authors:
        - Gabriel Ruiz

    :Last Modification:
        - 2017.08.14
    """
    student = models.ForeignKey(Student, related_name='final_grades',
                                on_delete=models.CASCADE)
    value = models.PositiveSmallIntegerField(_('Value'),
                                             null=True, blank=True)
    classes_qty = models.PositiveSmallIntegerField(_('ClassesQty'),
                                                   null=True, blank=True,
                                                   default=1)
    failing_qty = models.PositiveSmallIntegerField(_('FailingQty'),
                                                   null=True, blank=True,
                                                   default=0)
    year = models.PositiveSmallIntegerField(_('Value'),
                                            null=True, blank=True)
    created = models.DateTimeField(_('Created'), auto_now_add=True,
                                   editable=False)
    updated = models.DateTimeField(_('Updated'), auto_now=True)

    class Meta:
        app_label = 'school'
        verbose_name = _('StudentFinalGrade')
        verbose_name_plural = _('StudentFinalGrades')
        unique_together = ('student', 'year')
        ordering = ['id']

    def __str__(self):
        return "{} - {}".format(str(self.student) if self.student else '',
                                str(self.year))
