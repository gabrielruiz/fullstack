# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from .student import Student
from .school_class import SchoolClass


class StudentClassGrade(models.Model):
    """
    Modelo de datos para guardar los promedios de notas de
    cada alumno en cada asignatura. Se crea para no tener
    que recalcular el promedio cada vez que se revisan las
    notas, sino que solo se recalcule cuando haya un cambio
    en las notas de la asignatura en particular.

    Se asume que las notas siempre se aproxima, funcionando en
    un rango de 0 a 100.

    :Authors:
        - Gabriel Ruiz

    :Last Modification:
        - 2017.08.10
    """
    student = models.ForeignKey(Student, related_name='class_grades')
    school_class = models.ForeignKey(SchoolClass, related_name="student_class_grades")
    value = models.PositiveSmallIntegerField(_('Value'), null=True, blank=True)
    created = models.DateTimeField(_('Created'), auto_now_add=True,
                                   editable=False)
    updated = models.DateTimeField(_('Updated'), auto_now=True)

    class Meta:
        app_label = 'school'
        verbose_name = _('StudentClassGrade')
        verbose_name_plural = _('StudentClassGrade')
        ordering = ['id']

    def __str__(self):
        return "{} - {}".format(str(self.student),
                                str(self.school_class))
