# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from .school_class import SchoolClass


class SchoolTest(models.Model):
    """
    Modelo de datos para una prueba. Se le llamó 'SchoolTest' en
    lugar de 'Test' para evitar cualquier conflicto o confusión
    posible con las clases de Test de Django.

    :Authors:
        - Gabriel Ruiz

    :Last Modification:
        - 2017.08.10
    """
    school_class = models.ForeignKey(SchoolClass, related_name="tests")
    datetime = models.DateTimeField(_('DateTime'), null=False, blank=False)
    created = models.DateTimeField(_('Created'), auto_now_add=True,
                                   editable=False)
    updated = models.DateTimeField(_('Updated'), auto_now=True)

    class Meta:
        app_label = 'school'
        verbose_name = _('SchoolTest')
        verbose_name_plural = _('SchoolTests')
        ordering = ['id']

    def __str__(self):
        return str(self.school_class)
