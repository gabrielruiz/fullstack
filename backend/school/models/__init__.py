# -*- coding: utf-8 -*-
"""
'models' incluye todos los modelos creados para ejecutar la aplicación.
A diferenncia de la estructura clásica de Django, se hace que cada
modelo tenga un archivo por separado (similar al manejo de clases en
aplicacón Java), para facilitar su mantención.
"""

from .student import Student
from .school_class import SchoolClass
from .school_test import SchoolTest
from .student_grade import StudentGrade
from .student_class_grade import StudentClassGrade
from .final_grade import FinalGrade
