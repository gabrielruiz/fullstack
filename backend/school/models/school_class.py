# -*- coding: utf-8 -*-
"""
La clase SchoolClass describe los atributos de un curso.

IMPORTANTE: Sobre la relación M2M de alumnos y cursos, es
posible hacerlo de dos formas:

- Crear una clase en particular (por ejemplo, 'StudentsInClass')
que desacople la relación M2M en dos.
- Ocupar la clase ManyToManyField de Django para describir esta
relación. Se ocupará esta clase en este caso, para aprovechar
mejor las características que ofrece Django.

"""
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from .student import Student


class SchoolClass(models.Model):
    """
    Modelo de datos para un curso. Se le llamó 'SchoolClass' en
    lugar de 'Class' para evitar cualquier conflicto o confusión
    posible con la palabra reservada 'class'.
    En este caso, se asume que un 'curso' se refiere a una clase
    estilo universitario, es decir, de una materia en particular.
    Se piensa así a causa del postulado de que 'cada alumno puede
    estar en más de un curso'.

    Al eliminar la clase el atributo 'Teacher' queda como NULL, para
    que otro profesor pueda tomar el curso.
    Se asume que cada registro de curso corresponde solo a un año, para
    mantener un correcto registro de los alumnos que cada curso ha tenido
    en el tiempo.
    Para hacer tracking de los cambios de profesor, se puede ocupar
    una librería como django-simple-history.

    :Authors:
        - Gabriel Ruiz

    :Last Modification:
        - 2017.08.11
    """
    teacher = models.ForeignKey(User, related_name='school_classes',
                                on_delete=models.SET_NULL, null=True, blank=True)
    subject = models.CharField(_('Subject'), max_length=200, null=False,
                               blank=False)
    code = models.CharField(_('ClassCode'), max_length=20, null=False,
                            blank=False, unique=True)
    year = models.PositiveSmallIntegerField(_('Year'), null=False, blank=False)
    students = models.ManyToManyField(Student, related_name="classes", blank=True)
    created = models.DateTimeField(_('Created'), auto_now_add=True,
                                   editable=False)
    updated = models.DateTimeField(_('Updated'), auto_now=True)

    class Meta:
        app_label = 'school'
        verbose_name = _('SchoolClass')
        verbose_name_plural = _('SchoolClass')
        ordering = ['id']

    def __str__(self):
        return "{} {}".format(self.subject, self.code)
