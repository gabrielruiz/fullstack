# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Student(models.Model):
    """
    Modelo de datos para un alumno. No se ocupa la clase User como base,
    considerando que los usuarios no tienen permisos para acceder al
    sistema y con considerados solo como 'registros'.
    Para efectos prácticos, no se consideran datos como RUT, fecha de
    matrícula y otros, que no son necesarios en este caso.

    :Authors:
        - Gabriel Ruiz

    :Last Modification:
        - 2017.08.10
    """
    GENDER = (
        ('f', _('Female')),
        ('m', _('Male'))
    )
    name = models.CharField(_('Name'), max_length=100, null=False, blank=False)
    lastname_1 = models.CharField(_('Lastname1'), max_length=100, null=False, blank=False)
    lastname_2 = models.CharField(_('Lastname2'), max_length=100, null=False, blank=False)
    gender = models.CharField(_('Gender'), choices=GENDER, max_length=1)
    birthday = models.DateField(_('Birthday'), null=True, blank=True)
    created = models.DateTimeField(_('Created'), auto_now_add=True,
                                   editable=False)
    updated = models.DateTimeField(_('Updated'), auto_now=True)


    class Meta:
        app_label = 'school'
        verbose_name = _('Student')
        verbose_name_plural = _('Students')
        ordering = ['id']

    def __str__(self):
        return "{} {} {}".format(self.name, self.lastname_1, self.lastname_2)
