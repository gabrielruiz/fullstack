# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from school.models import Student, SchoolClass, SchoolTest, StudentGrade, \
    StudentClassGrade, FinalGrade
from school.serializers import StudentSerializer, SchoolClassSerializer, \
    SchoolTestSerializer, StudentGradeSerializer, StudentClassGradeSerializer,\
    FinalGradeSerializer, UserSerializer
from rest_framework import viewsets


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

    def perform_create(self, serializer):
        serializer.save()


class SchoolClassViewSet(viewsets.ModelViewSet):
    queryset = SchoolClass.objects.all()
    serializer_class = SchoolClassSerializer

    def perform_create(self, serializer):
        serializer.save()


class SchoolTestViewSet(viewsets.ModelViewSet):
    queryset = SchoolTest.objects.all()
    serializer_class = SchoolTestSerializer

    def perform_create(self, serializer):
        serializer.save()


class StudentGradeViewSet(viewsets.ModelViewSet):
    queryset = StudentGrade.objects.all()
    serializer_class = StudentGradeSerializer

    def perform_create(self, serializer):
        serializer.save()


class StudentClassGradeViewSet(viewsets.ModelViewSet):
    queryset = StudentClassGrade.objects.all()
    serializer_class = StudentClassGradeSerializer

    def perform_create(self, serializer):
        serializer.save()


class FinalGradeViewSet(viewsets.ModelViewSet):
    queryset = FinalGrade.objects.all()
    serializer_class = FinalGradeSerializer

    def perform_create(self, serializer):
        serializer.save()


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.filter(is_active=True, is_superuser=False)
    serializer_class = UserSerializer

    def perform_create(self, serializer):
        serializer.save()
