## Supuestos

* Los profesores son los usuarios del sistema con permisos para subir notas. Estos son creados solo desde el Admin.
* El listado de los alumnos con sus promedios se puede realizar a través del admin o desde la plataforma en la página inicial.
* En este caso, se asume que un 'curso' se refiere a una clase estilo universitario, es decir, de una ateria en particular. Se piensa así a causa del postulado de que 'cada alumno puede estar en más de un curso'.
* Se asume que cada registro de curso corresponde solo a un año, para mantener un correcto registro de los alumnos que cada curso ha tenido en el tiempo. Por ende, en la creación del curso y en el cálculo de promedios, se ocupa el año.
* En el registro de los Alumnos, para efectos prácticos, no se consideran datos como RUT, fecha de matrícula y otros, que no son necesarios en este caso.
* Se asume que las notas solo se truncan, funcionando en un rango de 0 a 100.

## Dependencias

* Python >= 3.4
* Django 1.11.4
* Django Rest Framework 3.6.3

El resto de las dependencias se encuentran descritas en el archivo `requirements.txt`

## Sobre la estructura del proyecto:

* No se sigue estrictamente la estructura sugerida por Django en las carpetas. En la app 'school', se crea una carpeta especial para los modelos (llamada 'models') y se elimina `models.py`. Los modelos se registran cada uno en un archivo, para facilitar su lectura y modificación, y son importados en `__init__.py`, de forma de que la importación en otros archivos siga siendo `from school.models import MyClass`. De esta forma, en proyectos muy grandes, se evita tener cientos o miles de líneas de código que revisar en un archivo `models.py`.
* En los tests se sigue la misma estructura anterior.

## Ejecución

Antes de ejecutar el proyecto, es necesario realizar la migración de la base de datos, rellenarla con algunos datos y crear un superusuario para poder acceder al admin. La base de datos elegida fue simplemente SQLite, suficiente para el test.

```
cd backend
python manage.py migrate
python manage.py loaddata users
python manage.py createsuperuser
```

Luego, se puede ejecutar normalmente con `python manage.py runserver`.

### Notas

* Al ingresar a la plataforma, solo se verán inicialmente los promedios de los alumnos. En la barra de navegación, existe la opción de autenticarse en la plataforma.
* Una vez autenticado, es posible ver las listas de alumnos, notas, pruebas y cursos.
* Una de las funcionalidades no implementadas, es el correcto uso de la paginación. Si bien está implementado en las APIs, no está siendo ocupado en la vista en frontend, por lo que se mostrarán hasta 20 objetos de cada tipo, por ahora.
* En el ingreso de fechas, no se ha implementado un calendario con Fecha/hora aún, así que se deben ingresar en el formato `2017-01-31T20:00`.
* Faltó implementar un forma de asignar los alumnos a los cursos, por lo que ahora solo es posible hacerlo desde el Admin de Django.