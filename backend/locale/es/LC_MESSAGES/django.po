# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-08-12 15:45+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: school/admin.py:16
msgid "FailingGrades"
msgstr "Promedios rojos"

#: school/models/school_class.py:47
msgid "Subject"
msgstr "Asignatura"

#: school/models/school_class.py:49
msgid "ClassCode"
msgstr "Código de curso"

#: school/models/school_class.py:51
msgid "Year"
msgstr "Año"

#: school/models/school_class.py:53 school/models/school_test.py:21
#: school/models/student.py:29 school/models/student_class_grade.py:28
#: school/models/student_grade.py:22
msgid "Created"
msgstr "Creado"

#: school/models/school_class.py:55 school/models/school_test.py:23
#: school/models/student.py:31 school/models/student_class_grade.py:30
#: school/models/student_grade.py:24
msgid "Updated"
msgstr "Actualizado"

#: school/models/school_class.py:59 school/models/school_class.py:60
msgid "SchoolClass"
msgstr "Curso"

#: school/models/school_test.py:20
msgid "DateTime"
msgstr "Fecha/Hora"

#: school/models/school_test.py:27
msgid "SchoolTest"
msgstr "Prueba"

#: school/models/school_test.py:28
msgid "SchoolTests"
msgstr "Pruebas"

#: school/models/student.py:21
msgid "Female"
msgstr "Femenino"

#: school/models/student.py:22
msgid "Male"
msgstr "Masculino"

#: school/models/student.py:24
msgid "Name"
msgstr "Nombre"

#: school/models/student.py:25
msgid "Lastname1"
msgstr "Primer apellido"

#: school/models/student.py:26
msgid "Lastname2"
msgstr "Segundo apellido"

#: school/models/student.py:27
msgid "Gender"
msgstr "Génerp"

#: school/models/student.py:28
msgid "Birthday"
msgstr "Fecha de nacimiento"

#: school/models/student.py:36
msgid "Student"
msgstr "Alumno"

#: school/models/student.py:37
msgid "Students"
msgstr "Alumnos"

#: school/models/student_class_grade.py:27 school/models/student_grade.py:21
msgid "Value"
msgstr "Valor"

#: school/models/student_class_grade.py:34
#: school/models/student_class_grade.py:35
msgid "StudentClassGrade"
msgstr "Promedio en el curso"

#: school/models/student_grade.py:28
msgid "StudentGrade"
msgstr "Nota"

#: school/models/student_grade.py:29
msgid "StudentGrades"
msgstr "Notas"
